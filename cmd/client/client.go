package main

import (
	"io/ioutil"
	"log"
	"strings"

	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/M0du1e/andrm/internal/ui/style"
	widgets "gitlab.com/M0du1e/andrm/internal/ui/widgets"
	licenses "gitlab.com/M0du1e/andrm/pkg/licenses_client"
)

func init() {
	//licenses.CheckLicense("http://127.0.0.1:8080/", false, false)
}

func handleCheck(license string) {

}

func showInfo(msg string, parent gtk.IWindow) {
	dialog := gtk.MessageDialogNew(
		parent,
		gtk.DIALOG_DESTROY_WITH_PARENT,
		gtk.MESSAGE_INFO,
		gtk.BUTTONS_OK,
		"%s",
		msg,
	)
	dialog.Run()
	dialog.Destroy()
}

func readLicenseFile() (string, error) {
	license, err := ioutil.ReadFile("license.dat")
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(string(license)), nil
}

func ui() {
	gtk.Init(nil)

	window, _ := gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
	window.SetTitle("AnDRM client")

	box, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 6)
	label, _ := gtk.LabelNew("License:")
	label.SetHAlign(gtk.ALIGN_START)
	entry, _ := gtk.EntryNew()

	box.SetMarginStart(style.MARGIN_LARGE)
	box.SetMarginTop(style.MARGIN_LARGE)
	box.SetMarginEnd(style.MARGIN_LARGE)
	box.SetMarginBottom(style.MARGIN_LARGE)

	license, err := readLicenseFile()

	if err != nil {
		log.Println("Failed to read license.dat:", err)
	} else {
		entry.SetText(license)
	}

	checkButton, _ := gtk.ButtonNewWithLabel("Check License")

	box.Add(label)
	box.Add(entry)
	box.Add(checkButton)
	box.SetVExpand(true)

	window.Add(box)
	window.ShowAll()

	window.Connect("destroy", func() {
		gtk.MainQuit()
	})

	checkButton.Connect("clicked", func() {
		licenseText, _ := entry.GetText()

		result, err := licenses.CheckLicense("127.0.0.1", 5455, false, licenseText)

		if err != nil {
			log.Println("Check license error:", err)
			widgets.ShowError(err.Error(), window)
		} else {
			log.Println("Check license result:", result)
			widgets.ShowInfo(result, window)
		}
	})

	gtk.Main()
}

func main() {
	/*fmt.Println("License was varified!")
	for {
	}*/
	ui()
}
