package products

import (
	"errors"
	"fmt"

	"gitlab.com/M0du1e/andrm/internal/logger"
)

// GetProduct finds one product by id
func GetProduct(id uint) (*Product, error) {
	products, err := GetProducts(&[]uint{id})

	if err != nil {
		msg := "Failed to get products:" + err.Error()
		logger.Error(msg)
		return nil, errors.New(msg)
	}

	if len(products) == 0 {
		msg := fmt.Sprintf("Product id=%d not found", id)
		logger.Error(msg)
		return nil, errors.New(msg)
	}

	product := products[0]

	return product, nil
}
