package products

import types "gitlab.com/M0du1e/andrm/internal/products/types"

type Product struct {
	ID     uint
	Type   *types.ProductType
	Name   string
	MinAge uint8
}
