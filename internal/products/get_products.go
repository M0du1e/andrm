package products

import (
	"errors"
	"fmt"
	"log"
	"strings"

	"gitlab.com/M0du1e/andrm/internal/config"
	"gitlab.com/M0du1e/andrm/internal/logger"
	types "gitlab.com/M0du1e/andrm/internal/products/types"
)

// GetProducts gets products
func GetProducts(ids *[]uint) ([]*Product, error) {
	logger.Debug("Get products...")

	query := "SELECT id, type_id, name, min_age FROM products"
	args := []interface{}{}

	if ids != nil && len(*ids) > 0 {
		idStrings := []string{}

		for _, id := range *ids {
			idStrings = append(idStrings, fmt.Sprint(id))
		}

		query += fmt.Sprintf(" WHERE id IN (%s)", strings.Join(idStrings, ","))
	}

	log.Println("SQL Query:", query)
	db := config.DB
	rows, err := db.Query(query, args...)
	if err != nil {
		msg := "Failed to get products:" + err.Error()
		log.Print(msg)
		return nil, errors.New(msg)
	}
	defer rows.Close()

	products := []*Product{}

	for rows.Next() {
		var (
			id     uint
			typeID uint
			name   string
			minAge uint8
		)
		if err := rows.Scan(&id, &typeID, &name, &minAge); err != nil {
			msg := "Failed to get products:" + err.Error()
			logger.Error(msg)
			return nil, errors.New(msg)
		}

		productType, err := types.GetProductType(typeID)

		if err != nil {
			msg := fmt.Sprintf("Failed to get product type (id = %d): %s", typeID, err.Error())
			logger.Error(msg)
			return nil, errors.New(msg)
		}

		products = append(products, &Product{
			id,
			productType,
			name,
			minAge,
		})
		logger.Info("Got product: id=%d, name=%s\n", id, name)
	}

	return products, nil
}
