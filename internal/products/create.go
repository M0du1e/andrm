package products

import (
	"errors"

	"gitlab.com/M0du1e/andrm/internal/config"
	"gitlab.com/M0du1e/andrm/internal/logger"
	types "gitlab.com/M0du1e/andrm/internal/products/types"
)

// CreateProduct creates new product
func CreateProduct(
	productType *types.ProductType,
	name string,
	minAge uint8,
) error {
	db := config.DB

	if name == "" {
		return errors.New("Name is required")
	}

	if minAge <= 0 {
		return errors.New("Age is required")
	}

	_, err := db.Exec(
		"INSERT INTO products (type_id, name, min_age) VALUES (?, ?, ?)",
		productType.ID, name, minAge,
	)

	if err != nil {
		msg := "Failed to create product:" + err.Error()
		logger.Error(msg)
		return errors.New(msg)
	}

	return nil
}
