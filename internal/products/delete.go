package products

import (
	"errors"
	"fmt"

	"gitlab.com/M0du1e/andrm/internal/config"
	"gitlab.com/M0du1e/andrm/internal/logger"
)

// DeleteProduct deletes product by ID
func DeleteProduct(productID uint) error {
	db := config.DB

	_, err := db.Exec("DELETE FROM products WHERE id=?", productID)

	if err != nil {
		msg := fmt.Sprintf("Failed to delete product (id = %d): %s", productID, err.Error())
		logger.Error(msg)
		return errors.New(msg)
	}

	return nil
}
