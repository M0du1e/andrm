package usersUI

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/M0du1e/andrm/internal/logger"
	licensesUIStore "gitlab.com/M0du1e/andrm/internal/ui/licenses/store"
	"gitlab.com/M0du1e/andrm/internal/ui/widgets"
	"gitlab.com/M0du1e/andrm/internal/users"
)

func (page *UsersPage) handleDelete() {
	logger.Debug("Delete user...")
	userID, err := page.getSelectedID()

	if err != nil {
		widgets.ShowError(err.Error(), nil)
		return
	}

	confirm := widgets.ShowConfirm("Are you sure you want to delete user?", nil)

	if confirm != gtk.RESPONSE_YES {
		return
	}

	err = users.DeleteUser(userID)

	if err != nil {
		widgets.ShowError(err.Error(), nil)
	}

	Store.Refresh()
	licensesUIStore.Store.Refresh()
}
