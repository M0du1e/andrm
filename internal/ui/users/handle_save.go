package usersUI

import (
	"time"

	"gitlab.com/M0du1e/andrm/internal/ui/widgets"
	"gitlab.com/M0du1e/andrm/internal/users"
	"gitlab.com/M0du1e/andrm/internal/users/genders"
)

func (editUserDialog *EditUserDialog) handleSave() {
	user := editUserDialog.User
	dialog := editUserDialog.Dialog
	emailEntry := editUserDialog.EmailEntry
	nameEntry := editUserDialog.NameEntry
	genderComboBox := editUserDialog.GenderComboBox

	email := emailEntry.GetText()
	name := nameEntry.GetText()
	genderID, err := genderComboBox.GetID()

	if err != nil {
		widgets.ShowError(err.Error(), dialog)
		return
	}

	gender, err := genders.GetGender(genderID)

	if err != nil {
		widgets.ShowError(err.Error(), dialog)
		return
	}

	year, month, day := editUserDialog.BirthDateCalendar.GetDate()

	user.Email = email
	user.Name = name
	user.BirthDate = time.Date(int(year), time.Month(int(month)+1), int(day), 0, 0, 0, 0, time.UTC)
	user.Gender = gender

	err = users.UpdateUser(user)

	if err != nil {
		widgets.ShowError(err.Error(), dialog)
		return
	}

	Store.Refresh()
	dialog.Destroy()
}
