package gendersUI

import (
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/M0du1e/andrm/internal/logger"
	"gitlab.com/M0du1e/andrm/internal/ui/widgets"
	"gitlab.com/M0du1e/andrm/internal/users/genders"
)

var Store GendersStore = GendersStoreNew()

type GendersStore struct {
	Genders   []*genders.Gender
	ListStore *gtk.ListStore
}

const (
	GENDER_COLUMN_ID = iota
	GENDER_COLUMN_NAME
)

func GendersStoreNew() GendersStore {
	listStore, _ := gtk.ListStoreNew(
		glib.TYPE_STRING, // ID
		glib.TYPE_STRING, // Name
	)
	return GendersStore{
		make([]*genders.Gender, 0),
		listStore,
	}
}

func (*GendersStore) Refresh() {
	genders, err := genders.GetGenders()

	if err != nil {
		widgets.ShowError("Failed to get genders: "+err.Error(), nil)
		return
	}

	Store.Clear()

	for _, gender := range genders {
		Store.Add(gender)
	}
}

func (store *GendersStore) Clear() {
	store.ListStore.Clear()
}

func (*GendersStore) Add(gender *genders.Gender) {
	id := int(gender.ID)
	name := gender.Name

	listStore := Store.ListStore
	// Get an iterator for a new row at the end of the list store
	iter := listStore.Append()

	// Set the contents of the list store row that the iterator represents
	err := listStore.Set(iter,
		[]int{
			GENDER_COLUMN_ID,
			GENDER_COLUMN_NAME,
		},
		[]interface{}{
			id,
			name,
		},
	)

	if err != nil {
		logger.Error("Unable to add row:", err)
	}
}
