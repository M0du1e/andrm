package usersUI

import (
	"fmt"

	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/M0du1e/andrm/internal/ui/style"
	"gitlab.com/M0du1e/andrm/internal/ui/widgets"
	"gitlab.com/M0du1e/andrm/internal/users"
)

type EditUserDialog struct {
	User              *users.User
	Dialog            *gtk.Dialog
	EmailEntry        *widgets.EntryWithLabel
	NameEntry         *widgets.EntryWithLabel
	GenderComboBox    *widgets.ComboBox
	BirthDateCalendar *gtk.Calendar
}

func EditUserDialogNew(user *users.User) EditUserDialog {
	appWindow := widgets.Widgets["appWindow"].(*gtk.ApplicationWindow)
	dialog, _ := gtk.DialogNewWithButtons(
		fmt.Sprintf("Edit user %s", user),
		appWindow,
		gtk.DIALOG_MODAL,
		[]interface{}{"Save", gtk.RESPONSE_ACCEPT}, []interface{}{"Cancel", gtk.RESPONSE_CANCEL},
	)
	dialogBox, _ := dialog.GetContentArea()
	box, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, style.MARGIN_SMALL)

	dialogBox.SetMarginStart(style.MARGIN_LARGE)
	dialogBox.SetMarginTop(style.MARGIN_LARGE)
	dialogBox.SetMarginEnd(style.MARGIN_LARGE)
	dialogBox.SetMarginBottom(style.MARGIN_LARGE)
	dialogBox.SetSpacing(style.MARGIN_LARGE)

	emailEntry := widgets.EntryWithLabelNew("Email:")
	emailEntry.SetText(user.Email)
	nameEntry := widgets.EntryWithLabelNew("Name:")
	nameEntry.SetText(user.Name)

	genderComboBox := getGenderComboBox()
	genderComboBox.SetID(user.Gender.ID)

	calendar, _ := gtk.CalendarNew()

	calendar.SelectMonth(uint(user.BirthDate.Month())-1, uint(user.BirthDate.Year()))
	calendar.SelectDay(uint(user.BirthDate.Day()))

	calendarLabel, _ := gtk.LabelNew("Date of birth:")
	calendarLabel.SetHAlign(gtk.ALIGN_START)
	calendarBox, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, style.MARGIN_SMALL)

	calendarBox.Add(calendarLabel)
	calendarBox.Add(calendar)

	box.Add(emailEntry.Container())
	box.Add(nameEntry.Container())
	box.Add(genderComboBox.Container())
	box.Add(calendarBox)

	dialogBox.Add(box)

	editUserDialog := EditUserDialog{
		user,
		dialog,
		&emailEntry,
		&nameEntry,
		&genderComboBox,
		calendar,
	}

	dialog.Connect("response", func(_ *gtk.Dialog, response gtk.ResponseType) {
		if response != gtk.RESPONSE_ACCEPT {
			dialog.Destroy()
			return
		}

		editUserDialog.handleSave()
	})

	return editUserDialog
}

func (dialog *EditUserDialog) Run() {
	dialog.Dialog.ShowAll()
}
