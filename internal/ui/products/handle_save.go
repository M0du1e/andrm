package productsUI

import (
	"fmt"

	"gitlab.com/M0du1e/andrm/internal/products"
	producttypes "gitlab.com/M0du1e/andrm/internal/products/types"
	store "gitlab.com/M0du1e/andrm/internal/ui/products/store"
	"gitlab.com/M0du1e/andrm/internal/ui/widgets"
)

func (editProductDialog *EditProductDialog) handleSave() {
	product := editProductDialog.Product
	dialog := editProductDialog.Dialog
	typeComboBox := editProductDialog.TypeComboBox
	nameEntry := editProductDialog.NameEntry
	minAgeSpin := editProductDialog.MinAgeSpin

	typeID, err := typeComboBox.GetID()
	if err != nil {
		widgets.ShowError(fmt.Sprintf("Type is required: %s", err.Error()), dialog)
		return
	}

	name := nameEntry.GetText()
	minAge := minAgeSpin.GetValue()

	productType, err := producttypes.GetProductType(typeID)
	if err != nil {
		widgets.ShowError(err.Error(), dialog)
		return
	}

	product.Type = productType
	product.Name = name
	product.MinAge = minAge

	err = products.UpdateProduct(product)

	if err != nil {
		widgets.ShowError(err.Error(), dialog)
		return
	}

	store.Store.Refresh()
	dialog.Destroy()
}
