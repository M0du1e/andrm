package productsUI

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/M0du1e/andrm/internal/products"
	store "gitlab.com/M0du1e/andrm/internal/ui/products/store"
	"gitlab.com/M0du1e/andrm/internal/ui/widgets"
)

// Add a column to the tree view (during the initialization of the tree view)
func createProductColumn(title string, id int) *gtk.TreeViewColumn {
	cellRenderer, _ := gtk.CellRendererTextNew()

	column, _ := gtk.TreeViewColumnNewWithAttribute(title, cellRenderer, "text", id)

	return column
}

type ProductsPage struct {
	Box      *gtk.Box
	TreeView *gtk.TreeView
}

func (page *ProductsPage) getSelectedID() (uint, error) {
	return widgets.GetTreeViewSelectedID(
		page.TreeView,
		store.Store.ListStore,
		store.PRODUCTS_COLUMN_ID,
	)
}

func BuildProductsPage() gtk.IWidget {
	box, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)

	createIcon, _ := gtk.ImageNewFromIconName("list-add", gtk.ICON_SIZE_SMALL_TOOLBAR)
	createButton, _ := gtk.ToolButtonNew(createIcon, "Add")

	editIcon, _ := gtk.ImageNewFromIconName("document-edit-symbolic", gtk.ICON_SIZE_SMALL_TOOLBAR)
	editButton, _ := gtk.ToolButtonNew(editIcon, "Edit")

	deleteIcon, _ := gtk.ImageNewFromIconName("delete", gtk.ICON_SIZE_SMALL_TOOLBAR)
	deleteButton, _ := gtk.ToolButtonNew(deleteIcon, "Delete")

	sep, _ := gtk.SeparatorToolItemNew()

	refreshIcon, _ := gtk.ImageNewFromIconName("view-refresh", gtk.ICON_SIZE_SMALL_TOOLBAR)
	refreshButton, _ := gtk.ToolButtonNew(refreshIcon, "Refresh")

	toolbar, _ := gtk.ToolbarNew()

	toolbar.Add(createButton)
	toolbar.Add(editButton)
	toolbar.Add(deleteButton)

	toolbar.Add(sep)

	toolbar.Add(refreshButton)

	box.Add(toolbar)

	treeView, _ := gtk.TreeViewNew()
	treeView.AppendColumn(createProductColumn("ID", store.PRODUCTS_COLUMN_ID))
	nameColumn := createProductColumn("Product", store.PRODUCTS_COLUMN_NAME)
	nameColumn.SetExpand(true)
	treeView.AppendColumn(nameColumn)
	treeView.AppendColumn(createProductColumn("Type", store.PRODUCTS_COLUMN_TYPE))
	treeView.AppendColumn(createProductColumn("Min Age", store.PRODUCTS_COLUMN_MIN_AGE))

	treeView.SetModel(store.Store.ListStore)
	treeView.SetHExpand(true)
	treeView.SetVExpand(true)
	treeView.SetReorderable(true)
	treeView.SetEnableSearch(true)
	treeView.SetSearchColumn(store.PRODUCTS_COLUMN_NAME)

	box.Add(treeView)

	page := ProductsPage{
		box,
		treeView,
	}

	/* Actions */
	store.Store.Refresh()

	refreshButton.Connect("clicked", func() {
		store.Store.Refresh()
	})

	createButton.Connect("clicked", func() {
		addProductDialog := AddProductDialogNew()
		addProductDialog.Run()
	})

	deleteButton.Connect("clicked", func() {
		page.handleDelete()
	})

	editButton.Connect("clicked", func() {
		productID, err := page.getSelectedID()

		if err != nil {
			widgets.ShowError(err.Error(), nil)
			return
		}

		product, err := products.GetProduct(productID)

		if err != nil {
			widgets.ShowError(err.Error(), nil)
			return
		}

		editDialog := EditProductDialogNew(product)
		editDialog.Run()
	})

	return box
}
