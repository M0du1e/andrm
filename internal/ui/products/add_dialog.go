package productsUI

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/M0du1e/andrm/internal/ui/style"
	"gitlab.com/M0du1e/andrm/internal/ui/widgets"
)

type AddProductDialog struct {
	Dialog       *gtk.Dialog
	TypeComboBox *widgets.ComboBox
	NameEntry    *widgets.EntryWithLabel
	MinAgeSpin   *widgets.SpinWithLabel
}

func AddProductDialogNew() AddProductDialog {
	appWindow := widgets.Widgets["appWindow"].(*gtk.ApplicationWindow)
	dialog, _ := gtk.DialogNewWithButtons(
		"Add product",
		appWindow,
		gtk.DIALOG_DESTROY_WITH_PARENT,
		[]interface{}{"Add", gtk.RESPONSE_ACCEPT},
		[]interface{}{"Cancel", gtk.RESPONSE_CANCEL},
	)
	dialogBox, _ := dialog.GetContentArea()
	box, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, style.MARGIN_SMALL)

	dialogBox.SetMarginStart(style.MARGIN_LARGE)
	dialogBox.SetMarginTop(style.MARGIN_LARGE)
	dialogBox.SetMarginEnd(style.MARGIN_LARGE)
	dialogBox.SetMarginBottom(style.MARGIN_LARGE)
	dialogBox.SetSpacing(style.MARGIN_LARGE)

	nameEntry := widgets.EntryWithLabelNew("Name:")

	ageSpin := widgets.SpinWithLabelNew("Min age:", 12, 21)
	ageSpin.Spin.SetValue(18)

	typeComboBox := getTypeComboBox()

	box.Add(typeComboBox.Container())
	box.Add(nameEntry.Container())
	box.Add(ageSpin.Container())

	dialogBox.Add(box)

	productAddDialog := AddProductDialog{
		dialog,
		&typeComboBox,
		&nameEntry,
		&ageSpin,
	}

	dialog.Connect("response", func(_ *gtk.Dialog, response gtk.ResponseType) {
		if response != gtk.RESPONSE_ACCEPT {
			dialog.Destroy()
			return
		}

		productAddDialog.handleAdd()
	})

	return productAddDialog
}

func (dialog *AddProductDialog) Run() {
	dialog.Dialog.ShowAll()
}
