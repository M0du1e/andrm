package licensesUi

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/M0du1e/andrm/internal/config"
	"gitlab.com/M0du1e/andrm/internal/licenses"
	"gitlab.com/M0du1e/andrm/internal/ui/style"
	"gitlab.com/M0du1e/andrm/internal/ui/widgets"
)

type EditLicenseDialog struct {
	License         *licenses.License
	Dialog          *gtk.Dialog
	UserComboBox    *widgets.ComboBox
	ProductComboBox *widgets.ComboBox
	Calendar        *gtk.Calendar
}

func EditLicenseDialogNew(license *licenses.License) EditLicenseDialog {
	appWindow := widgets.Widgets["appWindow"].(*gtk.ApplicationWindow)
	dialog, _ := gtk.DialogNewWithButtons(
		"Edit license",
		appWindow,
		gtk.DIALOG_DESTROY_WITH_PARENT,
		[]interface{}{"Save", gtk.RESPONSE_ACCEPT},
		[]interface{}{"Cancel", gtk.RESPONSE_CANCEL},
	)
	dialogBox, _ := dialog.GetContentArea()
	box, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, style.MARGIN_SMALL)

	dialogBox.SetMarginStart(style.MARGIN_LARGE)
	dialogBox.SetMarginTop(style.MARGIN_LARGE)
	dialogBox.SetMarginEnd(style.MARGIN_LARGE)
	dialogBox.SetMarginBottom(style.MARGIN_LARGE)
	dialogBox.SetSpacing(style.MARGIN_LARGE)

	userComboBox := getUserComboBox()
	userComboBox.SetID(license.User.ID)

	productComboBox := getProductComboBox()
	productComboBox.SetID(license.Product.ID)

	calendar, _ := gtk.CalendarNew()

	calendar.SelectMonth(uint(license.Expiration.Month())-1, uint(license.Expiration.Year()))
	calendar.SelectDay(uint(license.Expiration.Day()))

	calendarLabel, _ := gtk.LabelNew("Expiration:")
	calendarLabel.SetHAlign(gtk.ALIGN_START)
	calendarBox, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, style.MARGIN_SMALL)

	calendarBox.Add(calendarLabel)
	calendarBox.Add(calendar)

	licenseEntry := widgets.EntryWithLabelNew("License:")
	licenseEntry.SetText(license.License)
	licenseEntry.Entry.SetEditable(false)

	keyEntry := widgets.EntryWithLabelNew("User key:")
	key, _ := licenses.Encrypt(config.KEY, license.License)
	keyEntry.SetText(key)
	keyEntry.Entry.SetEditable(false)

	box.Add(userComboBox.Container())
	box.Add(productComboBox.Container())
	box.Add(calendarBox)
	box.Add(licenseEntry.Container())
	box.Add(keyEntry.Container())

	dialogBox.Add(box)

	editDialog := EditLicenseDialog{
		license,
		dialog,
		&userComboBox,
		&productComboBox,
		calendar,
	}

	dialog.Connect("response", func(_ *gtk.Dialog, response gtk.ResponseType) {
		if response != gtk.RESPONSE_ACCEPT {
			dialog.Destroy()
			return
		}

		editDialog.handleSave()
	})

	return editDialog
}

func (dialog *EditLicenseDialog) Run() {
	dialog.Dialog.ShowAll()
}
