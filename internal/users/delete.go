package users

import (
	"errors"
	"fmt"

	"gitlab.com/M0du1e/andrm/internal/config"
	"gitlab.com/M0du1e/andrm/internal/logger"
)

func DeleteUser(userID uint) error {
	db := config.DB

	_, err := db.Exec("DELETE FROM users WHERE id=?", userID)

	if err != nil {
		msg := fmt.Sprintf("Failed to delete user (id = %d): %s", userID, err.Error())
		logger.Error(msg)
		return errors.New(msg)
	}

	return nil
}
