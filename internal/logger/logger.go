package logger

import "log"

func Error(args ...interface{}) {
	logArgs := append([]interface{}{"[ERROR]"}, args...)
	log.Println(logArgs...)
}

func Warn(args ...interface{}) {
	logArgs := append([]interface{}{"[WARN]"}, args...)
	log.Println(logArgs...)
}

func Info(args ...interface{}) {
	logArgs := append([]interface{}{"[INFO]"}, args...)
	log.Println(logArgs...)
}

func Debug(args ...interface{}) {
	logArgs := append([]interface{}{"[DEBUG]"}, args...)
	log.Println(logArgs...)
}

func Trace(args ...interface{}) {
	logArgs := append([]interface{}{"[TRACE]"}, args...)
	log.Println(logArgs...)
}
