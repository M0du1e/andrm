package licenses

import (
	"errors"
	"fmt"
	"log"
	"time"

	"gitlab.com/M0du1e/andrm/internal/config"
	"gitlab.com/M0du1e/andrm/internal/logger"
	"gitlab.com/M0du1e/andrm/internal/products"
	"gitlab.com/M0du1e/andrm/internal/users"
)

// GetLicense gets one license by id
func GetLicense(id uint) (*License, error) {
	logger.Debug(fmt.Sprintf("Get license (id=%d)...", id))

	db := config.DB

	query := "SELECT id, user_id, product_id, license, expiration FROM licenses WHERE id=?"

	row := db.QueryRow(query, id)

	var (
		userID     uint
		productID  uint
		key        string
		expiration time.Time
	)
	if err := row.Scan(&id, &userID, &productID, &key, &expiration); err != nil {
		msg := "Failed to get license:" + err.Error()
		logger.Error(msg)
		return nil, errors.New(msg)
	}

	user, err := users.GetUser(userID)
	if err != nil {
		return nil, err
	}

	product, err := products.GetProduct(productID)
	if err != nil {
		return nil, err
	}

	license := &License{
		id,
		user,
		product,
		key,
		expiration,
	}
	log.Printf("Got license: id=%d for user user_id=%d\n", id, userID)

	return license, nil
}
