package licenses

import (
	"errors"
	"fmt"

	"gitlab.com/M0du1e/andrm/internal/config"
	"gitlab.com/M0du1e/andrm/internal/logger"
)

// UpdateLicense updates license data in DB
func UpdateLicense(license *License) error {
	user := license.User
	product := license.Product

	userAge := user.Age()
	if userAge < product.MinAge {
		msg := fmt.Sprintf(
			"Product age restriction violation: min age is %d, user age is %d",
			product.MinAge, userAge,
		)
		return errors.New(msg)
	}

	db := config.DB

	userID := user.ID
	productID := product.ID

	_, err := db.Exec(
		`UPDATE licenses SET user_id=?, product_id=?, expiration=? WHERE id=?`,
		userID, productID, license.Expiration.Format("2006-01-02"), license.ID,
	)

	if err != nil {
		msg := "Failed to update license in database: " + err.Error()
		logger.Error(msg)
		return errors.New(msg)
	}

	return nil
}
